package facci.angelinepico.projectmovil;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import facci.angelinepico.projectmovil.Modelo.Users;

public class registro extends AppCompatActivity {

    TextView login, nombre, apellido, dirreccion, email, contrasena;
    Button registro;
    FirebaseAuth auth;
    DatabaseReference reference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_registro);

        auth = FirebaseAuth.getInstance();
        login = findViewById(R.id.login);
        registro = findViewById(R.id.Registro);
        nombre = findViewById(R.id.Nombre);
        apellido= findViewById(R.id.Apellido);
        dirreccion= findViewById(R.id.direccion);
        email= findViewById(R.id.Email);
        contrasena= findViewById(R.id.contrasena);



        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(registro.this, login.class));
                finish();
            }
        });

        registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String txt_nombre = nombre.getText().toString();
                String txt_apellido = apellido.getText().toString();
                String txt_direccion = dirreccion.getText().toString();
                String txt_email = email.getText().toString();
                String txt_contrasena = contrasena.getText().toString();

                if (TextUtils.isEmpty(txt_nombre) || TextUtils.isEmpty(txt_apellido) || TextUtils.isEmpty(txt_direccion) || TextUtils.isEmpty(txt_email) || TextUtils.isEmpty(txt_contrasena)) {
                    Toast.makeText(registro.this, "Ningun campo debe estar vacio", Toast.LENGTH_SHORT).show();
                } else if (txt_contrasena.length() < 6) {
                    Toast.makeText(registro.this, "ERROR: La contraseña debe tener almenos 6 caracteres", Toast.LENGTH_SHORT).show();
                } else {

                    register(txt_nombre, txt_apellido, txt_direccion, txt_email, txt_contrasena);
                }

            }
        });
        
    }

    private void register(final String nombre, final String apellido, final String direccion, final String email, String contrasena) {

        auth.createUserWithEmailAndPassword(email, contrasena)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            FirebaseUser firebaseUser = auth.getCurrentUser();
                            assert  firebaseUser != null;
                            String userid = firebaseUser.getUid();
                            reference = FirebaseDatabase.getInstance().getReference("usuarios").child(userid);
                            Users users = new Users();
                            users.setUid(userid);
                            users.setNombre(nombre);
                            users.setApellido(apellido);
                            users.setDirreccion(direccion);
                            users.setEmail(email);

                            reference.setValue(users).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()){
                                        Intent intent = new Intent(registro.this, principal.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            });
                        } else {
                            Toast.makeText(registro.this, "tienes problemas con el correo", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

}
